﻿Imports ESRI.ArcGIS.Carto
Imports ESRI.ArcGIS.Geodatabase

Public Class frmstyle2layerSetting
    Private _layer As IFeatureLayer = Nothing
    Public Sub setlayer(layer As IFeatureLayer)
        _layer = layer
    End Sub
    Private _AxTOCControl1 As ESRI.ArcGIS.Controls.AxTOCControl = Nothing
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Dispose()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Button1.Enabled = False
        Dim v As Dictionary(Of String, String) = getValueLabelDict(txtLabelFile.Text)
        Dim name_value As Dictionary(Of String, String) = getValueLabelDict(txtSymbolValue.Text)
        Style2mxd.GetSymbol(txtStyle.Text,
                            "Fillsymbols"，
                            _layer,
                            v, chkUseCategory.Checked, chkAddnameTOLabel.Checked,
                            chkClearExistSymbol.Checked, chkUpdateLabel.Checked, name_value)
        _AxTOCControl1.Update()
        Button1.Enabled = True
    End Sub
    Private Function getValueLabelDict(path As String) As Dictionary(Of String, String)
        Dim res As New Dictionary(Of String, String)
        If Not System.IO.File.Exists(path) Then
            Return res
        End If
        Dim fs As New System.IO.FileStream(path, IO.FileMode.Open)
        Dim sw As New System.IO.StreamReader(fs)
        Dim line As String = sw.ReadLine
        While (line IsNot Nothing)
            Dim lineSplit() As String = line.Split(",，、".ToCharArray, StringSplitOptions.RemoveEmptyEntries)
            If lineSplit.Length = 2 Then
                Dim v As String = lineSplit(0).Trim.ToLower
                Dim l As String = lineSplit(1).Trim
                If res.ContainsKey(v) = False Then
                    res.Add(v, l)
                End If
            End If
            line = sw.ReadLine
        End While
        sw.Close()
        fs.Close()
        Return res
    End Function
    Sub New(layer As IFeatureLayer, AxTOCControl1 As ESRI.ArcGIS.Controls.AxTOCControl)
        _layer = layer
        _AxTOCControl1 = AxTOCControl1
        ' 此调用是设计器所必需的。
        InitializeComponent()

        ' 在 InitializeComponent() 调用之后添加任何初始化。
        'cmbFields.Items.Clear()
        'Dim ftrcls As IFeatureClass = layer.FeatureClass
        'Dim flds As IFields = ftrcls.Fields
        'For i As Integer = 0 To flds.FieldCount - 1
        '    Dim fld As IField
        '    fld = flds.Field(i)
        '    If fld.Type = esriFieldType.esriFieldTypeGeometry Then
        '        Continue For
        '    End If
        '    cmbFields.Items.Add(fld.Name)
        'Next
    End Sub
    Private Sub frmstyle2layerSetting_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        OpenFileDialog1.Filter = "*.style|*.style"
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            txtStyle.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        OpenFileDialog1.Filter = "*.txt|*.txt"
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            txtLabelFile.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub chkClearExistSymbol_CheckedChanged(sender As Object, e As EventArgs) Handles chkClearExistSymbol.CheckedChanged

    End Sub

    Private Sub frmstyle2layerSetting_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        'Me.Hide()
        'e.Cancel = True
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        OpenFileDialog1.Filter = "*.txt;*.csv|*.txt;*.csv"
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            txtSymbolValue.Text = OpenFileDialog1.FileName
        End If
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim dlg As New SaveFileDialog
        dlg.Filter = "*.csv|*.csv"
        If dlg.ShowDialog = DialogResult.OK Then
            Button6.Enabled = False
            Style2mxd.exportSymbolStyleName(txtStyle.Text,
                                "Fillsymbols"，
                                _layer, dlg.FileName)
            Button6.Enabled = True
        End If

    End Sub
End Class