﻿Imports ESRI.ArcGIS.Carto

Public Class frmStyle2Layer
    Private Sub frmStyle2Layer_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub AxTOCControl1_OnMouseDown(sender As Object, e As ESRI.ArcGIS.Controls.ITOCControlEvents_OnMouseDownEvent) Handles AxTOCControl1.OnMouseDown
        If e.button <> 2 Then
            Return
        End If
        Dim p = New System.Drawing.Point(e.x, e.y)
        Dim layer As ILayer = Nothing
        Dim other As Object = Nothing
        Dim index As Object = Nothing
        Dim map As IMap = Nothing
        Try
            AxTOCControl1.HitTest(e.x, e.y,
                                          ESRI.ArcGIS.Controls.esriTOCControlItem.esriTOCControlItemLayer,
                                          map,
                                          layer, other, index)
        Catch ex As Exception

        End Try

        AxTOCControl1.Tag = layer
        If layer Is Nothing Then
            ' MessageBox.Show("未选中要素层")
            Return
        End If
        ContextMenuStrip1.Show(AxTOCControl1, p.X, p.Y)
    End Sub
    Private _dlg As frmstyle2layerSetting = Nothing
    Private Sub ToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ToolStripMenuItem1.Click
        If AxTOCControl1.Tag Is Nothing OrElse TypeOf AxTOCControl1.Tag IsNot IFeatureLayer Then
            MessageBox.Show("未选中要素层")
            Return
        End If
        Dim player As IGeoFeatureLayer = AxTOCControl1.Tag
        If Not TypeOf player.Renderer Is IUniqueValueRenderer Then
            MessageBox.Show("选择图层不是唯一值渲染，只支持唯一值渲染")
            Return
        End If
        If _dlg Is Nothing OrElse _dlg.IsDisposed Then
            _dlg = New frmstyle2layerSetting(player, AxTOCControl1)
            _dlg.Show(Me)
        Else
            If _dlg.WindowState = FormWindowState.Minimized Then
                _dlg.WindowState = FormWindowState.Normal
            End If
            _dlg.Visible = True
        End If
        _dlg.Text = String.Format("设置图层[{0}]的唯一值符号", player.Name)
        _dlg.setlayer(player)
    End Sub

    Private Sub frmStyle2Layer_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing

    End Sub
End Class