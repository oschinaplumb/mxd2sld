﻿Public Class frmMain
    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        txtTip.Text = ""
        If System.IO.Path.GetExtension(txtsldFIle.Text).ToLower <> ".sld" Then
            System.Windows.Forms.MessageBox.Show("【" & txtsldFIle.Text & "】不是合法的sld文件！")
            Return
        End If
        Dim direc As String = System.IO.Path.GetDirectoryName(txtsldFIle.Text)
        If System.IO.Directory.Exists(direc) = False Then
            System.Windows.Forms.MessageBox.Show("【" & direc & "】不是合法的sld目录！")
            Return
        End If
        btnExport.Enabled = False
        Dim maps As New List(Of ESRI.ArcGIS.Carto.IMap)
        If AxMapControl1.CheckMxFile(txtmxd.Text) Then
            AxMapControl1.LoadMxFile(txtmxd.Text)
            maps.Add(AxMapControl1.Map)
        Else
            maps = GetProcesseMaps()
        End If
        If maps.Count = 0 Then
            btnExport.Enabled = True
            System.Windows.Forms.MessageBox.Show("【" & txtmxd.Text & "】不是合法的mxd文件！")
            Return
        End If
        Dim txtPath As String = System.IO.Path.Combine(direc, "图层与要素类对应关系.txt")
        Try
            If System.IO.File.Exists(txtPath) Then
                System.IO.File.Delete(txtPath)
            End If
        Catch ex As Exception

        End Try
        Dim fs As New System.IO.FileStream(txtPath, IO.FileMode.OpenOrCreate)
        Dim sw As New System.IO.StreamWriter(fs, System.Text.Encoding.UTF8)
        For Each map In maps
            Dim pmxd As New ParserMXD(map, txtTip, True, chkScale.Checked, chkSetLabel.Checked)
            pmxd.MultiLayerMarkerSymbolAsImage = chkMultiToPicture.Checked
            pmxd.GeomFieldName = txtGeomField.Text.Trim
            Dim res = pmxd.AnalyseLayerSymbology(txtsldFIle.Text,
                                       chkPreserveWhiteSpace.Checked,
                                       chkForOne.Checked,
                                       chkDatasetName.Checked,
                                       chkOnlyExportVisibleLayer.Checked, chkLayerNameLower.Checked,
                                                 chkMergeToOneFeatureclass.checked
                                       )
            For Each line In res
                sw.WriteLine(line)
            Next
        Next
        sw.Close()
        fs.Close()
        btnExport.Enabled = True
    End Sub

    Private Sub btnFolder_Click(sender As Object, e As EventArgs) Handles btnFolder.Click
        Dim dlg As New SaveFileDialog
        dlg.Filter = "(*.sld)|*.sld"
        If dlg.ShowDialog = DialogResult.OK Then
            txtsldFIle.Text = dlg.FileName
        End If
    End Sub
    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Text = String.Format("Mxd格式符号转为sld格式(V{0})", getVersion())
    End Sub
    Private Function getVersion() As String
        Dim asm As System.Reflection.Assembly

        asm = System.Reflection.Assembly.GetExecutingAssembly()
        Dim sVersion As String = asm.GetName().Version.ToString()

        Return sVersion
    End Function

    Private Sub btnMxd_Click(sender As Object, e As EventArgs) Handles btnMxd.Click
        Dim dlg As New OpenFileDialog
        dlg.Filter = "(*.mxd)|*.mxd"
        If dlg.ShowDialog = DialogResult.OK Then
            txtmxd.Text = dlg.FileName
        End If
    End Sub

    Private Sub chkForOne_CheckedChanged(sender As Object, e As EventArgs) Handles chkForOne.CheckedChanged, chkOnlyExportVisibleLayer.CheckedChanged, chkMergeToOneFeatureclass.CheckedChanged
        If chkForOne.Checked Then
            chkDatasetName.Enabled = False
            chkMergeToOneFeatureclass.Enabled = False
        Else
            chkDatasetName.Enabled = True
            chkMergeToOneFeatureclass.Enabled = True
        End If
    End Sub

    Private Sub btnAbout_Click(sender As Object, e As EventArgs) Handles btnAbout.Click
        Dim dlg As New about
        dlg.ShowDialog(Me)
    End Sub

    Private Sub btnStyle2mxd_Click(sender As Object, e As EventArgs) Handles btnStyle2mxd.Click
        Dim dlg As New frmStyle2Layer
        dlg.Show(Me)
    End Sub
#Region "arcmap进程获取"
    Private Function GetProcesseMaps() As List(Of ESRI.ArcGIS.Carto.IMap)
        'Dim objArcGISProcess As New Process
        'For Each objProcess In Process.GetProcesses()
        '    If objProcess.ProcessName = "ArcMap" Then
        '        bSwitch = True
        '    End If
        'Next
        Dim result As New List(Of ESRI.ArcGIS.Carto.IMap)
        Dim m_ObjAppROT = New ESRI.ArcGIS.Framework.AppROT
        Dim count As Integer = m_ObjAppROT.Count
        If count = 0 Then
            'System.Windows.Forms.MessageBox.Show("请打开arcmap！")
            Return result
        End If
        For i As Integer = 0 To count - 1
            If Not (TypeOf m_ObjAppROT.Item(i) Is ESRI.ArcGIS.ArcMapUI.IMxApplication) Then
                Continue For
            End If
            Dim mObjectApp As ESRI.ArcGIS.Framework.IApplication = m_ObjAppROT.Item(i)
            Dim doc As ESRI.ArcGIS.ArcMapUI.IMxDocument = mObjectApp.Document
            If doc.FocusMap.LayerCount > 0 Then
                result.Add(doc.FocusMap)
            End If
        Next
        Return result
    End Function
#End Region
End Class
