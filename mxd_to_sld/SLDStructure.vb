﻿Public Class SLdParameter
    Public paraname As String = ""
    Public value As String = ""
    Public isStandAlone As Boolean = False
    Public propertyName As String = ""
    Public OtherPropertys As New Dictionary(Of String, String)
    ''' <summary>
    ''' 是否为CData，主要用于多字段连接标注
    ''' </summary>
    Public mCdata As Boolean = False
    Public isOGCPrefix As Boolean = False
    Sub New(name As String, v As String,
            Optional standalone As Boolean = False,
            Optional ByVal pPropertyName As String = "",
            Optional ByVal ogcPrefix As Boolean = False)
        propertyName = pPropertyName
        paraname = name
        value = v
        isStandAlone = standalone
        isOGCPrefix = ogcPrefix
    End Sub
    Sub addOtherProperty(propertyName As String, value As String)
        If (OtherPropertys.ContainsKey(propertyName) = False) Then
            OtherPropertys.Add(propertyName, value)
        End If
    End Sub
End Class
Public Class SLDSymbol
    Public symbolType As String = ""
    Public sldParamOrSymbols As New List(Of Object)
    Public isOGCPrefix As Boolean = False
    Public OtherPropertys As New Dictionary(Of String, String)
    'Public childSymbols As New List(Of SLDSymbol)
    Sub New(typ As String, Optional ByVal ogcPrefix As Boolean = False, Optional ByVal name_value As String = "")
        symbolType = typ
        isOGCPrefix = ogcPrefix
        If Not String.IsNullOrWhiteSpace(name_value) Then
            OtherPropertys.Add("name", name_value)
        End If
    End Sub
    Sub addOtherProperty(propertyName As String, value As String)
        If (OtherPropertys.ContainsKey(propertyName) = False) Then
            OtherPropertys.Add(propertyName, value)
        End If
    End Sub
End Class
Public Class SLDSymbolLayer
    Public SymbolsOrParas As New List(Of Object)
    Public Sub add(pSymbolOrPara As Object)
        SymbolsOrParas.Add(pSymbolOrPara)
    End Sub
    Sub New(pSymbolOrPara As Object)
        add(pSymbolOrPara)
    End Sub
    Sub New(pSymbolOrParass As List(Of Object))
        SymbolsOrParas.AddRange(pSymbolOrParass)
    End Sub

    Sub New()

    End Sub
End Class
Public Class SLDSymbolizer
    Public layers As New List(Of SLDSymbolLayer)
    Public Sub addLayer(layer As SLDSymbolLayer)
        layers.Add(layer)
    End Sub
End Class
''' <summary>
''' 通过SLDStyle的NewRule创建
''' </summary>
Public Class SLDRule
    Public ruleName As String = ""
    Public description As String = ""
    Public title As String = ""
    Public sql As String = ""
    Public symbolType As SLDStyle.SymbolizerType = SLDStyle.SymbolizerType.PolygonSymbolizer
    Public mSymbolizerOrParas As New List(Of Object)
    Public Sub add(symbolizerOrPara As Object)
        mSymbolizerOrParas.Add(symbolizerOrPara)
    End Sub
End Class
Public Class SLDStyle
    Public styleName As String = ""
    Public title As String = ""
    Public description As String = ""
    Public Enum SymbolizerType
        PolygonSymbolizer
        PointSymbolizer
        LineSymbolizer
        TextSymbolizer
    End Enum
    Public symbolType As SymbolizerType = SymbolizerType.PolygonSymbolizer
    Sub New(ruleType As SymbolizerType)
        symbolType = ruleType
    End Sub
    Public rules As New List(Of SLDRule)
    Public Function NewRule() As SLDRule
        Dim rule As New SLDRule
        rule.symbolType = symbolType
        Return rule
    End Function
End Class
Public Class SLDLayer
    Public datasetname As String = ""
    Public layerName As String = ""
    Public layerPath As String = ""
    Public description As String = ""
    'Public Transparency As Short = -1
    Public styles As New List(Of SLDStyle)
End Class
