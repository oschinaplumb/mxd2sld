﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class about
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblAbout1 = New System.Windows.Forms.Label()
        Me.lblAbout2 = New System.Windows.Forms.TextBox()
        Me.lblAbout3 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'lblAbout1
        '
        Me.lblAbout1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAbout1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAbout1.Font = New System.Drawing.Font("微软雅黑", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAbout1.Location = New System.Drawing.Point(12, 9)
        Me.lblAbout1.Name = "lblAbout1"
        Me.lblAbout1.Size = New System.Drawing.Size(904, 61)
        Me.lblAbout1.TabIndex = 1
        Me.lblAbout1.Text = "标题"
        Me.lblAbout1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAbout2
        '
        Me.lblAbout2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAbout2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAbout2.Font = New System.Drawing.Font("微软雅黑", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAbout2.Location = New System.Drawing.Point(12, 83)
        Me.lblAbout2.Multiline = True
        Me.lblAbout2.Name = "lblAbout2"
        Me.lblAbout2.ReadOnly = True
        Me.lblAbout2.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.lblAbout2.Size = New System.Drawing.Size(904, 224)
        Me.lblAbout2.TabIndex = 2
        Me.lblAbout2.Text = "描述"
        '
        'lblAbout3
        '
        Me.lblAbout3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAbout3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblAbout3.Font = New System.Drawing.Font("微软雅黑", 10.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lblAbout3.Location = New System.Drawing.Point(12, 326)
        Me.lblAbout3.Multiline = True
        Me.lblAbout3.Name = "lblAbout3"
        Me.lblAbout3.ReadOnly = True
        Me.lblAbout3.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.lblAbout3.Size = New System.Drawing.Size(904, 76)
        Me.lblAbout3.TabIndex = 3
        Me.lblAbout3.Text = "描述"
        '
        'about
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(928, 414)
        Me.Controls.Add(Me.lblAbout3)
        Me.Controls.Add(Me.lblAbout2)
        Me.Controls.Add(Me.lblAbout1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "about"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "关于......"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblAbout1 As Label
    Friend WithEvents lblAbout2 As TextBox
    Friend WithEvents lblAbout3 As TextBox
End Class
