﻿Imports ESRI.ArcGIS.esriSystem
Namespace My
    Partial Friend Class MyApplication
        Private m_AOLicenseInitializer As LicenseInitializer = New mxd_to_sld.LicenseInitializer()

        Private Sub MyApplication_Startup(sender As Object, e As ApplicationServices.StartupEventArgs) Handles Me.Startup
            'ESRI License Initializer generated code.
            If (Not m_AOLicenseInitializer.InitializeApplication(New esriLicenseProductCode() {esriLicenseProductCode.esriLicenseProductCodeAdvanced},
            New esriLicenseExtensionCode() {})) Then
                MsgBox(m_AOLicenseInitializer.LicenseMessage() + vbNewLine + vbNewLine _
                + "This application could not initialize with the correct ArcGIS license and will shutdown.")
                m_AOLicenseInitializer.ShutdownApplication()
                e.Cancel = True
                Return
            End If
        End Sub

        Private Sub MyApplication_Shutdown(sender As Object, e As EventArgs) Handles Me.Shutdown
            'ESRI License Initializer generated code.
            'Do not make any call to ArcObjects after ShutDownApplication()
            m_AOLicenseInitializer.ShutdownApplication()
        End Sub
    End Class
End Namespace
