﻿Imports ESRI.ArcGIS.Carto
Imports ESRI.ArcGIS.Display

Public Class Style2mxd
    Public Shared Sub exportSymbolStyleName(sServerStylePath As String,
                          sGalleryClassName As String,
                                layer As IFeatureLayer, outpath As String)
        Dim pDataLayer As IDataLayer = layer
        Dim pFtrclsName As ESRI.ArcGIS.Geodatabase.IFeatureClassName = pDataLayer.DataSourceName
        If pFtrclsName.ShapeType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint Then
            sGalleryClassName = "Marker Symbols"
        ElseIf pFtrclsName.ShapeType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon Then
            sGalleryClassName = "Fill Symbols"
        ElseIf pFtrclsName.ShapeType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline Then
            sGalleryClassName = "Line Symbols"
        Else
            Return
        End If
        Dim pStyleGaller As IStyleGallery = New ESRI.ArcGIS.Framework.StyleGalleryClass()
        Dim pStyleGalleryStorage As IStyleGalleryStorage = TryCast(pStyleGaller, IStyleGalleryStorage)
        pStyleGalleryStorage.TargetFile = sServerStylePath
        Dim pStyleGalleryClass As IStyleGalleryClass = Nothing
        sGalleryClassName = sGalleryClassName.ToLower.Trim.Replace(" ", "")
        Dim fs As New IO.FileStream(outpath, IO.FileMode.OpenOrCreate)
        Dim sw As New IO.StreamWriter(fs)
        For i As Integer = 0 To pStyleGaller.ClassCount - 1
            pStyleGalleryClass = pStyleGaller.Class(i)
            If pStyleGalleryClass.Name.ToLower.Trim.Replace(" ", "") <> sGalleryClassName Then
                Continue For
            End If
            Dim pEnumSyleGalleryItem As IEnumStyleGalleryItem = pStyleGaller.Items(pStyleGalleryClass.Name, sServerStylePath, "")
            pEnumSyleGalleryItem.Reset()
            Dim pStyleGallerItem As IStyleGalleryItem = Nothing
            pStyleGallerItem = pEnumSyleGalleryItem.Next
            While pStyleGallerItem IsNot Nothing
                Dim category = pStyleGallerItem.Category
                Dim name = pStyleGallerItem.Name
                sw.WriteLine(String.Format("{0},{1}", name, category))
                pStyleGallerItem = pEnumSyleGalleryItem.Next
            End While
        Next
        sw.Close()
        fs.Close()
    End Sub
    Public Shared Sub GetSymbol(sServerStylePath As String,
                          sGalleryClassName As String,
                                layer As IFeatureLayer,
                                value_label As Dictionary(Of String, String),
                                useCategoryAsLabel As Boolean, addNameToCategory As Boolean,
                                clearExistSymbol As Boolean, updateLabelIfExist As Boolean, name_value As Dictionary(Of String, String))
        Dim pDataLayer As IDataLayer = layer
        Dim pFtrclsName As ESRI.ArcGIS.Geodatabase.IFeatureClassName = pDataLayer.DataSourceName
        If pFtrclsName.ShapeType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPoint Then
            sGalleryClassName = "Marker Symbols"
        ElseIf pFtrclsName.ShapeType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolygon Then
            sGalleryClassName = "Fill Symbols"
        ElseIf pFtrclsName.ShapeType = ESRI.ArcGIS.Geometry.esriGeometryType.esriGeometryPolyline Then
            sGalleryClassName = "Line Symbols"
        Else
            Return
        End If
        If value_label Is Nothing Then
            value_label = New Dictionary(Of String, String)
        End If
        Dim pGeoLayer As IGeoFeatureLayer = layer
        Dim renderer As IUniqueValueRenderer = pGeoLayer.Renderer
        If (clearExistSymbol) Then
            renderer.RemoveAllValues()
        End If
        Dim count As Integer = renderer.ValueCount
        Dim existValue As New Dictionary(Of String, Integer)
        For i As Integer = 0 To count - 1
            Dim name As String = renderer.Value(i)
            If Not (existValue.ContainsKey(name)) Then
                existValue.Add(name, i)
            End If
        Next
        'renderer.Field(0) = fieldname
        'Dim pStyleGaller = New ServerStyleGalleryClass()
        Dim pStyleGaller As IStyleGallery = New ESRI.ArcGIS.Framework.StyleGalleryClass()
        Dim pStyleGalleryStorage As IStyleGalleryStorage = TryCast(pStyleGaller, IStyleGalleryStorage)
        pStyleGalleryStorage.TargetFile = sServerStylePath
        Dim pStyleGalleryClass As IStyleGalleryClass = Nothing
        sGalleryClassName = sGalleryClassName.ToLower.Trim.Replace(" ", "")
        For i As Integer = 0 To pStyleGaller.ClassCount - 1
            pStyleGalleryClass = pStyleGaller.Class(i)
            If pStyleGalleryClass.Name.ToLower.Trim.Replace(" ", "") <> sGalleryClassName Then
                Continue For
            End If
            Dim pEnumSyleGalleryItem As IEnumStyleGalleryItem = pStyleGaller.Items(pStyleGalleryClass.Name, sServerStylePath, "")
            pEnumSyleGalleryItem.Reset()
            Dim pStyleGallerItem As IStyleGalleryItem = Nothing
            pStyleGallerItem = pEnumSyleGalleryItem.Next
            While pStyleGallerItem IsNot Nothing
                Dim psymbol As ISymbol
                psymbol = TryCast(pStyleGallerItem.Item, ISymbol)
                Dim category = pStyleGallerItem.Category
                Dim name = pStyleGallerItem.Name
                If name_value.ContainsKey(name.Trim.ToLower) Then
                    name = name_value.Item(name.Trim.ToLower)
                End If
                Dim nameLower As String = name.Trim.ToLower
                Dim label As String = pStyleGallerItem.Name
                If value_label.ContainsKey(nameLower) Then
                    label = value_label.Item(nameLower)
                Else
                    If useCategoryAsLabel Then
                        If addNameToCategory Then
                            label = name & category
                        Else
                            label = category
                        End If
                    Else
                        label = name
                    End If
                End If
                If (existValue.ContainsKey(name)) Then
                    If (updateLabelIfExist) Then
                        Dim index As Integer = existValue.Item(name)
                        'renderer.Label(index)
                        renderer.Label(name) = label
                        renderer.Symbol(name) = psymbol
                    End If
                Else
                    renderer.AddValue(name, "", psymbol)
                    renderer.Label(name) = label
                End If
                pStyleGallerItem = pEnumSyleGalleryItem.Next
            End While
        Next
        pGeoLayer.Renderer = renderer
    End Sub
End Class
