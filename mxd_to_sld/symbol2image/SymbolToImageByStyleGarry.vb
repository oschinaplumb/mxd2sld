﻿Imports ESRI.ArcGIS.Carto
Imports ESRI.ArcGIS.Display
Imports ESRI.ArcGIS.esriSystem
''' <summary>
''' 暂时未用，效果挺好，最大问题是不能设置背景透明，不能使用
''' </summary>
Public Class SymbolToImageByStyleGarry
    Private Shared image_dpi As Integer = 0
    Private Shared Sub getImageDPI()
        If image_dpi < 1 Then
            Dim b As New Bitmap(1, 1)
            Dim g As Graphics = Graphics.FromImage(b)
            image_dpi = g.DpiY
            b.Dispose()
            ' g.ReleaseHdc()
            g.Dispose()
        End If
    End Sub
    Public Shared Function GetImageFromSymbol(ByVal symbol As ISymbol) As Image
        getImageDPI()
        Dim width As Integer = 0
        Dim height As Integer = 0
        If TypeOf symbol Is IMarkerSymbol Then
            Dim size As Double = CType(symbol, IMarkerSymbol).Size
            width = size * image_dpi / 72 + 5
            height = width
        Else
            width = 64
            height = 64
        End If
        Dim styleGalleryClass As IStyleGalleryClass = Nothing

        If TypeOf symbol Is IMarkerSymbol Then
            styleGalleryClass = New MarkerSymbolStyleGalleryClass()
        ElseIf TypeOf symbol Is ILineSymbol Then
            styleGalleryClass = New LineSymbolStyleGalleryClass()
        ElseIf TypeOf symbol Is IFillSymbol Then
            styleGalleryClass = New FillSymbolStyleGalleryClassClass()
        End If

        If styleGalleryClass IsNot Nothing Then
            Return GetImage(styleGalleryClass, symbol, width, height)
        End If

        Return Nothing
    End Function

    Private Shared Function GetImage(ByVal styleGalleryClass As IStyleGalleryClass, ByVal symbol As ISymbol, ByVal width As Integer, ByVal height As Integer) As Image
        Dim img As Bitmap = New Bitmap(width, height)
        Dim GC As Graphics = Graphics.FromImage(img)
        Dim hdc As IntPtr = GC.GetHdc()
        Dim rect = New tagRECT()
        rect.left = 0
        rect.top = 0
        rect.right = width
        rect.bottom = height
        styleGalleryClass.Preview(symbol, hdc.ToInt32(), rect)
        GC.ReleaseHdc(hdc)
        GC.Dispose()
        Return img
    End Function
End Class
