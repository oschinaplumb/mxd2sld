﻿Imports System.IO
Imports System.Reflection

Public Class about
    Private Sub about_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim about1 As String = getText("about1.txt") + String.Format(" - 版本号：V{0}", getVersion)
        Dim about2 As String = getText("about2.txt")
        Dim about3 As String = getText("about3.txt")
        lblAbout1.Text = about1
        lblAbout2.Text = about2
        lblAbout3.Text = about3
        Me.Text = "关于" & about1 & "......"
    End Sub
    Private Function getText(fname As String) As String
        Dim t As Type = MethodBase.GetCurrentMethod().DeclaringType
        Dim _assembly As Assembly = Assembly.GetExecutingAssembly()
        Dim resourceName As String = t.Namespace + "." + fname
        Dim stream As Stream = _assembly.GetManifestResourceStream(resourceName)
        Dim txtReader As System.IO.StreamReader = New System.IO.StreamReader(stream)
        Dim v As String = txtReader.ReadToEnd()
        txtReader.Close()
        Return v
    End Function
    Private Function getVersion() As String
        Dim asm As System.Reflection.Assembly

        asm = System.Reflection.Assembly.GetExecutingAssembly()
        Dim sVersion As String = asm.GetName().Version.ToString()

        Return sVersion
    End Function
End Class