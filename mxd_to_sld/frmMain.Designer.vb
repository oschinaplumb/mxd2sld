﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.AxMapControl1 = New ESRI.ArcGIS.Controls.AxMapControl()
        Me.AxTOCControl1 = New ESRI.ArcGIS.Controls.AxTOCControl()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.txtTip = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtsldFIle = New System.Windows.Forms.TextBox()
        Me.btnFolder = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtGeomField = New System.Windows.Forms.TextBox()
        Me.chkScale = New System.Windows.Forms.CheckBox()
        Me.chkDatasetName = New System.Windows.Forms.CheckBox()
        Me.chkOnlyExportVisibleLayer = New System.Windows.Forms.CheckBox()
        Me.chkForOne = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkMultiToPicture = New System.Windows.Forms.CheckBox()
        Me.chkLayerNameLower = New System.Windows.Forms.CheckBox()
        Me.chkPreserveWhiteSpace = New System.Windows.Forms.CheckBox()
        Me.chkSetLabel = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtmxd = New System.Windows.Forms.TextBox()
        Me.btnMxd = New System.Windows.Forms.Button()
        Me.btnAbout = New System.Windows.Forms.Button()
        Me.btnStyle2mxd = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.chkMergeToOneFeatureclass = New System.Windows.Forms.CheckBox()
        CType(Me.AxMapControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxTOCControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'AxMapControl1
        '
        Me.AxMapControl1.Location = New System.Drawing.Point(190, 127)
        Me.AxMapControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.AxMapControl1.Name = "AxMapControl1"
        Me.AxMapControl1.OcxState = CType(resources.GetObject("AxMapControl1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxMapControl1.Size = New System.Drawing.Size(120, 116)
        Me.AxMapControl1.TabIndex = 0
        Me.AxMapControl1.Visible = False
        '
        'AxTOCControl1
        '
        Me.AxTOCControl1.Location = New System.Drawing.Point(41, 127)
        Me.AxTOCControl1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.AxTOCControl1.Name = "AxTOCControl1"
        Me.AxTOCControl1.OcxState = CType(resources.GetObject("AxTOCControl1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxTOCControl1.Size = New System.Drawing.Size(144, 97)
        Me.AxTOCControl1.TabIndex = 1
        Me.AxTOCControl1.Visible = False
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnExport.Font = New System.Drawing.Font("微软雅黑", 14.0!)
        Me.btnExport.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnExport.Location = New System.Drawing.Point(111, 356)
        Me.btnExport.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(220, 82)
        Me.btnExport.TabIndex = 3
        Me.btnExport.Text = "开始输出sld"
        Me.btnExport.UseVisualStyleBackColor = False
        '
        'txtTip
        '
        Me.txtTip.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTip.Location = New System.Drawing.Point(12, 445)
        Me.txtTip.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtTip.Multiline = True
        Me.txtTip.Name = "txtTip"
        Me.txtTip.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtTip.Size = New System.Drawing.Size(1157, 246)
        Me.txtTip.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(98, 18)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "输出文件："
        '
        'txtsldFIle
        '
        Me.txtsldFIle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtsldFIle.Location = New System.Drawing.Point(130, 16)
        Me.txtsldFIle.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtsldFIle.Name = "txtsldFIle"
        Me.txtsldFIle.Size = New System.Drawing.Size(911, 28)
        Me.txtsldFIle.TabIndex = 6
        Me.txtsldFIle.Text = "test.sld"
        '
        'btnFolder
        '
        Me.btnFolder.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnFolder.Location = New System.Drawing.Point(1047, 8)
        Me.btnFolder.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnFolder.Name = "btnFolder"
        Me.btnFolder.Size = New System.Drawing.Size(122, 44)
        Me.btnFolder.TabIndex = 3
        Me.btnFolder.Text = "浏览..."
        Me.btnFolder.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.txtGeomField)
        Me.GroupBox1.Controls.Add(Me.chkScale)
        Me.GroupBox1.Controls.Add(Me.chkDatasetName)
        Me.GroupBox1.Controls.Add(Me.chkOnlyExportVisibleLayer)
        Me.GroupBox1.Controls.Add(Me.chkMergeToOneFeatureclass)
        Me.GroupBox1.Controls.Add(Me.chkForOne)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.chkMultiToPicture)
        Me.GroupBox1.Controls.Add(Me.chkLayerNameLower)
        Me.GroupBox1.Controls.Add(Me.chkPreserveWhiteSpace)
        Me.GroupBox1.Controls.Add(Me.chkSetLabel)
        Me.GroupBox1.Location = New System.Drawing.Point(28, 127)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(1141, 225)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "参数设置"
        '
        'txtGeomField
        '
        Me.txtGeomField.Location = New System.Drawing.Point(1019, 178)
        Me.txtGeomField.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtGeomField.Name = "txtGeomField"
        Me.txtGeomField.Size = New System.Drawing.Size(100, 28)
        Me.txtGeomField.TabIndex = 6
        Me.txtGeomField.Text = "geom"
        '
        'chkScale
        '
        Me.chkScale.AutoSize = True
        Me.chkScale.Checked = True
        Me.chkScale.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkScale.Location = New System.Drawing.Point(237, 48)
        Me.chkScale.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkScale.Name = "chkScale"
        Me.chkScale.Size = New System.Drawing.Size(430, 22)
        Me.chkScale.TabIndex = 0
        Me.chkScale.Text = "考虑注记层的最大最小比例（minscale-maxscale)"
        Me.chkScale.UseVisualStyleBackColor = True
        '
        'chkDatasetName
        '
        Me.chkDatasetName.AutoSize = True
        Me.chkDatasetName.Checked = True
        Me.chkDatasetName.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDatasetName.Location = New System.Drawing.Point(688, 103)
        Me.chkDatasetName.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkDatasetName.Name = "chkDatasetName"
        Me.chkDatasetName.Size = New System.Drawing.Size(448, 22)
        Me.chkDatasetName.TabIndex = 0
        Me.chkDatasetName.Text = "按图层输出时以要素类名称命名（默认按图层名称）"
        Me.chkDatasetName.UseVisualStyleBackColor = True
        '
        'chkOnlyExportVisibleLayer
        '
        Me.chkOnlyExportVisibleLayer.AutoSize = True
        Me.chkOnlyExportVisibleLayer.Location = New System.Drawing.Point(688, 48)
        Me.chkOnlyExportVisibleLayer.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkOnlyExportVisibleLayer.Name = "chkOnlyExportVisibleLayer"
        Me.chkOnlyExportVisibleLayer.Size = New System.Drawing.Size(241, 22)
        Me.chkOnlyExportVisibleLayer.TabIndex = 0
        Me.chkOnlyExportVisibleLayer.Text = "只导出可见图层(visible)"
        Me.chkOnlyExportVisibleLayer.UseVisualStyleBackColor = True
        '
        'chkForOne
        '
        Me.chkForOne.AutoSize = True
        Me.chkForOne.Location = New System.Drawing.Point(237, 103)
        Me.chkForOne.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkForOne.Name = "chkForOne"
        Me.chkForOne.Size = New System.Drawing.Size(268, 22)
        Me.chkForOne.TabIndex = 0
        Me.chkForOne.Text = "所有图层合并保存为一个文件"
        Me.chkForOne.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(924, 184)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(98, 18)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "几何字段："
        '
        'chkMultiToPicture
        '
        Me.chkMultiToPicture.AutoSize = True
        Me.chkMultiToPicture.Location = New System.Drawing.Point(237, 184)
        Me.chkMultiToPicture.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkMultiToPicture.Name = "chkMultiToPicture"
        Me.chkMultiToPicture.Size = New System.Drawing.Size(664, 22)
        Me.chkMultiToPicture.TabIndex = 0
        Me.chkMultiToPicture.Text = "多图层点符号转换为图片（针对转换后套不上的符号MultiLayerMarkerSymbol）"
        Me.chkMultiToPicture.UseVisualStyleBackColor = True
        '
        'chkLayerNameLower
        '
        Me.chkLayerNameLower.AutoSize = True
        Me.chkLayerNameLower.Checked = True
        Me.chkLayerNameLower.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLayerNameLower.Location = New System.Drawing.Point(22, 184)
        Me.chkLayerNameLower.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkLayerNameLower.Name = "chkLayerNameLower"
        Me.chkLayerNameLower.Size = New System.Drawing.Size(196, 22)
        Me.chkLayerNameLower.TabIndex = 0
        Me.chkLayerNameLower.Text = "字段名称转化为小写"
        Me.chkLayerNameLower.UseVisualStyleBackColor = True
        '
        'chkPreserveWhiteSpace
        '
        Me.chkPreserveWhiteSpace.AutoSize = True
        Me.chkPreserveWhiteSpace.Location = New System.Drawing.Point(22, 103)
        Me.chkPreserveWhiteSpace.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkPreserveWhiteSpace.Name = "chkPreserveWhiteSpace"
        Me.chkPreserveWhiteSpace.Size = New System.Drawing.Size(160, 22)
        Me.chkPreserveWhiteSpace.TabIndex = 0
        Me.chkPreserveWhiteSpace.Text = "保存时去除空格"
        Me.chkPreserveWhiteSpace.UseVisualStyleBackColor = True
        '
        'chkSetLabel
        '
        Me.chkSetLabel.AutoSize = True
        Me.chkSetLabel.Checked = True
        Me.chkSetLabel.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSetLabel.Location = New System.Drawing.Point(22, 48)
        Me.chkSetLabel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkSetLabel.Name = "chkSetLabel"
        Me.chkSetLabel.Size = New System.Drawing.Size(106, 22)
        Me.chkSetLabel.TabIndex = 0
        Me.chkSetLabel.Text = "导出注记"
        Me.chkSetLabel.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(25, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 18)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "mxd路径："
        '
        'txtmxd
        '
        Me.txtmxd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtmxd.Location = New System.Drawing.Point(130, 72)
        Me.txtmxd.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtmxd.Name = "txtmxd"
        Me.txtmxd.Size = New System.Drawing.Size(911, 28)
        Me.txtmxd.TabIndex = 6
        Me.txtmxd.Text = "sld.mxd"
        '
        'btnMxd
        '
        Me.btnMxd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMxd.Location = New System.Drawing.Point(1047, 64)
        Me.btnMxd.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnMxd.Name = "btnMxd"
        Me.btnMxd.Size = New System.Drawing.Size(122, 44)
        Me.btnMxd.TabIndex = 3
        Me.btnMxd.Text = "浏览..."
        Me.btnMxd.UseVisualStyleBackColor = True
        '
        'btnAbout
        '
        Me.btnAbout.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAbout.Font = New System.Drawing.Font("微软雅黑", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.btnAbout.Location = New System.Drawing.Point(1047, 346)
        Me.btnAbout.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnAbout.Name = "btnAbout"
        Me.btnAbout.Size = New System.Drawing.Size(106, 50)
        Me.btnAbout.TabIndex = 8
        Me.btnAbout.Text = "关于"
        Me.btnAbout.UseVisualStyleBackColor = True
        '
        'btnStyle2mxd
        '
        Me.btnStyle2mxd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStyle2mxd.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnStyle2mxd.Font = New System.Drawing.Font("微软雅黑", 14.0!)
        Me.btnStyle2mxd.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.btnStyle2mxd.Location = New System.Drawing.Point(429, 356)
        Me.btnStyle2mxd.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnStyle2mxd.Name = "btnStyle2mxd"
        Me.btnStyle2mxd.Size = New System.Drawing.Size(465, 82)
        Me.btnStyle2mxd.TabIndex = 3
        Me.btnStyle2mxd.Text = "Style转到mxd唯一值渲染"
        Me.btnStyle2mxd.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(127, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(395, 18)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "mxd路径不存在或不设置时读取当前打开的arcmap"
        '
        'chkMergeToOneFeatureclass
        '
        Me.chkMergeToOneFeatureclass.AutoSize = True
        Me.chkMergeToOneFeatureclass.Checked = True
        Me.chkMergeToOneFeatureclass.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMergeToOneFeatureclass.Location = New System.Drawing.Point(22, 140)
        Me.chkMergeToOneFeatureclass.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkMergeToOneFeatureclass.Name = "chkMergeToOneFeatureclass"
        Me.chkMergeToOneFeatureclass.Size = New System.Drawing.Size(520, 22)
        Me.chkMergeToOneFeatureclass.TabIndex = 0
        Me.chkMergeToOneFeatureclass.Text = "同一要素类多个图层时符号合并为一个（否则输出多个符号）"
        Me.chkMergeToOneFeatureclass.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(1181, 716)
        Me.Controls.Add(Me.btnAbout)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtmxd)
        Me.Controls.Add(Me.txtsldFIle)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtTip)
        Me.Controls.Add(Me.btnMxd)
        Me.Controls.Add(Me.btnFolder)
        Me.Controls.Add(Me.btnStyle2mxd)
        Me.Controls.Add(Me.btnExport)
        Me.Controls.Add(Me.AxTOCControl1)
        Me.Controls.Add(Me.AxMapControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        CType(Me.AxMapControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxTOCControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents AxMapControl1 As ESRI.ArcGIS.Controls.AxMapControl
    Friend WithEvents AxTOCControl1 As ESRI.ArcGIS.Controls.AxTOCControl
    Friend WithEvents btnExport As Button
    Friend WithEvents txtTip As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtsldFIle As TextBox
    Friend WithEvents btnFolder As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents chkScale As CheckBox
    Friend WithEvents chkSetLabel As CheckBox
    Friend WithEvents chkPreserveWhiteSpace As CheckBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtmxd As TextBox
    Friend WithEvents btnMxd As Button
    Friend WithEvents chkForOne As CheckBox
    Friend WithEvents chkDatasetName As CheckBox
    Friend WithEvents btnAbout As Button
    Friend WithEvents chkOnlyExportVisibleLayer As CheckBox
    Friend WithEvents chkLayerNameLower As CheckBox
    Friend WithEvents chkMultiToPicture As CheckBox
    Friend WithEvents btnStyle2mxd As Button
    Friend WithEvents txtGeomField As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents chkMergeToOneFeatureclass As CheckBox
End Class
