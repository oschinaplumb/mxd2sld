﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmstyle2layerSetting
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.txtStyle = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.txtLabelFile = New System.Windows.Forms.TextBox()
        Me.chkUseCategory = New System.Windows.Forms.CheckBox()
        Me.chkAddnameTOLabel = New System.Windows.Forms.CheckBox()
        Me.chkClearExistSymbol = New System.Windows.Forms.CheckBox()
        Me.chkUpdateLabel = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSymbolValue = New System.Windows.Forms.TextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(180, 318)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(144, 56)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "设置"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(733, 318)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(144, 56)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "退出"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(111, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(107, 18)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "style文件："
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.Location = New System.Drawing.Point(911, 26)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(90, 34)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "浏览..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'txtStyle
        '
        Me.txtStyle.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtStyle.Location = New System.Drawing.Point(233, 26)
        Me.txtStyle.Name = "txtStyle"
        Me.txtStyle.Size = New System.Drawing.Size(660, 28)
        Me.txtStyle.TabIndex = 4
        Me.txtStyle.Text = "D:\工作\2022\国土空间规划\符号库\三调工作分类（颜色库，无框线）.style"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(48, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(179, 36)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "值对应标签：" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "（符号实际值,标签）"
        '
        'Button4
        '
        Me.Button4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button4.Location = New System.Drawing.Point(911, 78)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(90, 34)
        Me.Button4.TabIndex = 3
        Me.Button4.Text = "浏览..."
        Me.Button4.UseVisualStyleBackColor = True
        '
        'txtLabelFile
        '
        Me.txtLabelFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLabelFile.Location = New System.Drawing.Point(233, 78)
        Me.txtLabelFile.Name = "txtLabelFile"
        Me.txtLabelFile.Size = New System.Drawing.Size(660, 28)
        Me.txtLabelFile.TabIndex = 4
        Me.txtLabelFile.Text = "D:\工作\字段值标签.txt"
        '
        'chkUseCategory
        '
        Me.chkUseCategory.AutoSize = True
        Me.chkUseCategory.Checked = True
        Me.chkUseCategory.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUseCategory.Location = New System.Drawing.Point(141, 201)
        Me.chkUseCategory.Name = "chkUseCategory"
        Me.chkUseCategory.Size = New System.Drawing.Size(736, 22)
        Me.chkUseCategory.TabIndex = 5
        Me.chkUseCategory.Text = "标签值用Category内容显示（默认用name），在值对应标签.txt内容中有值时首先用该值"
        Me.chkUseCategory.UseVisualStyleBackColor = True
        '
        'chkAddnameTOLabel
        '
        Me.chkAddnameTOLabel.AutoSize = True
        Me.chkAddnameTOLabel.Checked = True
        Me.chkAddnameTOLabel.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAddnameTOLabel.Location = New System.Drawing.Point(141, 244)
        Me.chkAddnameTOLabel.Name = "chkAddnameTOLabel"
        Me.chkAddnameTOLabel.Size = New System.Drawing.Size(340, 22)
        Me.chkAddnameTOLabel.TabIndex = 5
        Me.chkAddnameTOLabel.Text = "标签值用Category内容显示时添加name"
        Me.chkAddnameTOLabel.UseVisualStyleBackColor = True
        '
        'chkClearExistSymbol
        '
        Me.chkClearExistSymbol.AutoSize = True
        Me.chkClearExistSymbol.Location = New System.Drawing.Point(141, 280)
        Me.chkClearExistSymbol.Name = "chkClearExistSymbol"
        Me.chkClearExistSymbol.Size = New System.Drawing.Size(142, 22)
        Me.chkClearExistSymbol.TabIndex = 5
        Me.chkClearExistSymbol.Text = "清除已有符号"
        Me.chkClearExistSymbol.UseVisualStyleBackColor = True
        '
        'chkUpdateLabel
        '
        Me.chkUpdateLabel.AutoSize = True
        Me.chkUpdateLabel.Checked = True
        Me.chkUpdateLabel.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUpdateLabel.Location = New System.Drawing.Point(388, 280)
        Me.chkUpdateLabel.Name = "chkUpdateLabel"
        Me.chkUpdateLabel.Size = New System.Drawing.Size(250, 22)
        Me.chkUpdateLabel.TabIndex = 5
        Me.chkUpdateLabel.Text = "符号存在时更新符号和标签"
        Me.chkUpdateLabel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 136)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(224, 36)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "符号库名称与值对应关系：" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "（符号库名称,实际值）"
        '
        'txtSymbolValue
        '
        Me.txtSymbolValue.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSymbolValue.Location = New System.Drawing.Point(233, 133)
        Me.txtSymbolValue.Name = "txtSymbolValue"
        Me.txtSymbolValue.Size = New System.Drawing.Size(660, 28)
        Me.txtSymbolValue.TabIndex = 4
        Me.txtSymbolValue.Text = "D:\工作\符号库名称与实际值对应关系.txt"
        '
        'Button5
        '
        Me.Button5.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button5.Location = New System.Drawing.Point(911, 127)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(90, 34)
        Me.Button5.TabIndex = 3
        Me.Button5.Text = "浏览..."
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(462, 318)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(144, 56)
        Me.Button6.TabIndex = 0
        Me.Button6.Text = "导出符号库名称和标签"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'frmstyle2layerSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 18.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 416)
        Me.Controls.Add(Me.chkUpdateLabel)
        Me.Controls.Add(Me.chkClearExistSymbol)
        Me.Controls.Add(Me.chkAddnameTOLabel)
        Me.Controls.Add(Me.chkUseCategory)
        Me.Controls.Add(Me.txtSymbolValue)
        Me.Controls.Add(Me.txtLabelFile)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.txtStyle)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button1)
        Me.Name = "frmstyle2layerSetting"
        Me.Text = "设置"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents txtStyle As TextBox
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents Label3 As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents txtLabelFile As TextBox
    Friend WithEvents chkUseCategory As CheckBox
    Friend WithEvents chkAddnameTOLabel As CheckBox
    Friend WithEvents chkClearExistSymbol As CheckBox
    Friend WithEvents chkUpdateLabel As CheckBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtSymbolValue As TextBox
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
End Class
