﻿Imports ESRI.ArcGIS.Display
Public Class SldUnit
    '1 px = 0.75 point

    Private Const point As Double = 1
    Private Const px As Double = 0.75 * point
    Public Shared Function Point2Pixel(point As Double) As Double
        Return point '/ px
    End Function
    Public Shared Function MarkerPoint2Pixel(point As Double) As Double
        Return point '/ px
    End Function
End Class
Module CommonFunctions
#Region "获取颜色字符串"
    Public Function getStringForColor(ByVal color As IColor) As String
        Dim cCol As String
        Dim cRed, cGreen, cBlue As String
        Dim objRGB As IRgbColor

        If (color.Transparency = 0) Then
            cCol = ""
        Else
            objRGB = New RgbColor

            objRGB.RGB = color.RGB
            cRed = CheckDigits(Hex(objRGB.Red()).ToString)
            cGreen = CheckDigits(Hex(objRGB.Green()).ToString)
            cBlue = CheckDigits(Hex(objRGB.Blue()).ToString)
            cCol = "#" & cRed & cGreen & cBlue
        End If

        Return cCol
    End Function
    Private Function CheckDigits(ByVal value As String) As String
        Dim cReturn As String
        cReturn = value
        If cReturn.Length = 1 Then
            cReturn = cReturn.Insert(0, "0")
        End If
        Return cReturn
    End Function
#End Region
    ''' <summary>
    ''' arcgis的angle与geoserver的rotation方向不一致，arcgis逆时针，geoserver顺时针，需要转换
    ''' </summary>
    ''' <param name="angle"></param>
    ''' <returns></returns>
    Public Function getRotation(angle As Double) As Double
        Dim nAngle As Double = 360 - angle
        If nAngle < 0 Then
            nAngle += 360
        ElseIf nAngle > 360 Then
            nAngle -= 360
        End If
        Return Math.Round(nAngle, 2)
    End Function
End Module
